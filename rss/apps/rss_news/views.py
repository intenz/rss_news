from django.http import HttpResponse
from django.shortcuts import render
from rss.apps.rss_news.models import Post
import random


# Create your views here.

def index(request):
    post = reversed(Post.objects.all())
    context = {
        'posts': post
    }
    return render(request, 'index.html', context)


def post(request, index):
    try:
        post = Post.objects.get(id=index)
        result = post.text
    except Post.DoesNotExist:
        result = "does not exist"
    return HttpResponse(result)
